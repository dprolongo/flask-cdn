Flask-CDN
=========

[![Version](https://img.shields.io/pypi/v/flask-cdn.svg)](https://pypi.python.org/pypi/flask-cdn)
[![Build Status](https://travis-ci.org/wichitacode/flask-cdn.png)](https://travis-ci.org/wichitacode/flask-cdn)
[![Coverage Status](https://coveralls.io/repos/wichitacode/flask-cdn/badge.svg)](https://coveralls.io/r/wichitacode/flask-cdn)

Serve the static files in your Flask app from a CDN.

Documentation
-------------
The latest documentation for Flask-CDN can be found [here](https://flask-cdn.readthedocs.org/en/latest/).
